package com.example.krystian.tetrisgame;

import android.app.Activity;
import android.graphics.Color;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends Activity {

    final int h = 14;
    final int v = 20;

    Boolean[][] arr = new Boolean[22][22];
    Boolean[][] tmpArr = new Boolean[22][22];
    boolean generateBlock = true;

    Block block = new Block();
    Boolean[][] blockArray = new Boolean[3][3];
    String fieldName = "";

    int blockH = 0;

    boolean start = false;
    int direction = 5;
    boolean changeDirection = true;
    int vertical = 1;
    int prevFieldName = 0;
    TextView prevField = null;
    TextView field = null;

    private Button startButton;

    private TextView timerValue;

    private long startTime = 0L;

    private Handler customHandler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for(int i=0; i < 22; i++){
            for(int j=0; j < 22; j++){
                arr[i][j] = false;
                tmpArr[i][j] = false;
            }
        }

        final Button right = (Button) findViewById(R.id.right);
        final Button left = (Button) findViewById(R.id.left);
        timerValue = (TextView) findViewById(R.id.textView);
        startButton = (Button) findViewById(R.id.change);

        right.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(changeDirection && direction < h-3) direction++;
                changeDirection = false;
            }
        });

        left.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(changeDirection && direction > 0) direction--;
                changeDirection = false;
            }
        });

        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(start) {
                    customHandler.removeCallbacks(updateTimerThread);
                    startButton.setText("START");
                    start = false;
                }
                else{
                    startTime = SystemClock.uptimeMillis();
                    customHandler.postDelayed(updateTimerThread, 1000);
                    startButton.setText("STOP");
                    start = true;
                }
            }
        });

    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            if(generateBlock){
                blockArray = block.getRandomBlock();
                generateBlock = false;
            }

            clearScreen();

            for(int i = 0; i < 3; i++){
                for(int j = 0; j < 3; j++) {

                    if(blockArray[i][j]) {
                        tmpArr[i + vertical][j +1+ direction] = true;
                        System.out.print("pole"+(i + vertical)+"_"+(j +1+ direction));
                        System.out.println("");
                    }
                }
            }

            for(int i = 0; i < 3; i++){
                for(int j = 0; j < 3; j++) {

                    if(blockArray[i][j]) {
                        fieldName = "pole" + (i+vertical) + "_" +(j+1+direction);
                        int resId = getResources().getIdentifier(fieldName, "id", getPackageName());
                        field = (TextView) findViewById(resId);
                        field.setBackgroundColor(Color.parseColor("#ffffff"));
                        blockH = i;
                    }

                }
            }
            fillSavedBlocks();

            if(vertical+blockH+1 > 20) {
                saveBlock(blockArray, vertical, direction);
                generateBlock = true;
                vertical = 0;

            }

            if(checkScreen(tmpArr)){

                generateBlock = true;
                vertical = 0;
            }

            vertical++;

            System.out.println(vertical);
            changeDirection = true;
            customHandler.postDelayed(this, 1000);
        }

    };

    public void clearScreen() {

        for(int i=1; i < 21; i++){
            for(int j=1; j < 15; j++) {
                fieldName = "pole" + (i) + "_" +(j);
                int resId = getResources().getIdentifier(fieldName, "id", getPackageName());
                field = (TextView) findViewById(resId);
                field.setBackgroundColor(Color.parseColor("#4bb39e"));
            }
        }

        for(int i=0; i < 22; i++){
            for(int j=0; j < 22; j++){
                tmpArr[i][j] = false;
            }
        }
    }

    public void saveBlock(Boolean[][] array, int x, int y){

        System.out.println("X: "+x+" Y:"+y);
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++) {

                if(array[i][j]) {
                    arr[i+x][j+1+y] = true;
                }

            }
        }
    }

    public void fillSavedBlocks() {

        for(int i=0; i < 22; i++){
            for(int j=0; j < 22; j++) {

                if(arr[i][j]) {
                    fieldName = "pole" + (i) + "_" +(j);
                    System.out.println(fieldName);
                    int resId = getResources().getIdentifier(fieldName, "id", getPackageName());
                    field = (TextView) findViewById(resId);
                    field.setBackgroundColor(Color.parseColor("#ffffff"));
                }
            }
        }
    }

    public boolean checkScreen(Boolean[][] array) {
        for(int i=0; i < 22; i++){
            for(int j=0; j < 22; j++){
                if(arr[i][j] && array[i][j]){
                    return true;
                }
            }
        }
        return false;
    }

}
