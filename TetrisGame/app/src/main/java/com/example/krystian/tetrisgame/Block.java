package com.example.krystian.tetrisgame;

import java.util.Random;

/**
 * Created by Krystian on 2015-06-21.
 */
public class Block {

    public Boolean[][] getRandomBlock() {
        Boolean[][] array = new Boolean[3][3];
        int x = this.randomType();
        /**********
         * Klocki:
         * 1 - Kwadrat 2x2
         * 2 - Kreska 1x3
         * 3 - Kreska 3x1
         * 4 - elka #1
         * 5 - elka #2
         * 6 - elka #3
         * 7 - elka #4
         * 8 - "_-"
         * 9 - "-_"
         */

        if(x == 1) {
            array[0][0] = true;     array[0][1] = true;    array[0][2] = true;
            array[1][0] = true;     array[1][1] = true;    array[1][2] = true;
            array[2][0] = true;     array[2][1] = true;    array[2][2] = true;
        }
        else if(x == 2){
            array[0][0] = false;     array[0][1] = true;    array[0][2] = false;
            array[1][0] = false;     array[1][1] = true;    array[1][2] = false;
            array[2][0] = false;     array[2][1] = true;    array[2][2] = false;
        }
        else if(x == 3){
            array[0][0] = false;     array[0][1] = false;    array[0][2] = false;
            array[1][0] = true;      array[1][1] = true;     array[1][2] = true;
            array[2][0] = false;     array[2][1] = false;    array[2][2] = false;
        }
        else if(x == 4){
            array[0][0] = true;     array[0][1] = false;    array[0][2] = false;
            array[1][0] = true;     array[1][1] = false;    array[1][2] = false;
            array[2][0] = true;     array[2][1] = true;     array[2][2] = true;
        }
        else if(x == 5){
            array[0][0] = true;     array[0][1] = true;     array[0][2] =  true;
            array[1][0] = true;     array[1][1] = false;    array[1][2] = false;
            array[2][0] = true;     array[2][1] = false;    array[2][2] = false;
        }
        else if(x == 6){
            array[0][0] = true;     array[0][1] = true;     array[0][2] =  true;
            array[1][0] = false;    array[1][1] = false;    array[1][2] = true;
            array[2][0] = false;    array[2][1] = false;    array[2][2] = true;
        }
        else if(x == 7){
            array[0][0] = false;     array[0][1] = false;    array[0][2] =  true;
            array[1][0] = false;     array[1][1] = false;    array[1][2] = true;
            array[2][0] = true;      array[2][1] = true;     array[2][2] = true;
        }
        else if(x == 8){
            array[0][0] = false;    array[0][1] = true;    array[0][2] =  true;
            array[1][0] = true;    array[1][1] = true;    array[1][2] = false;
            array[2][0] = false;    array[2][1] = false;    array[2][2] = false;
        }
        else if(x == 9){
            array[0][0] = true;     array[0][1] = true;    array[0][2] =  false;
            array[1][0] = false;     array[1][1] = true;    array[1][2] = true;
            array[2][0] = false;     array[2][1] = false;    array[2][2] = false;
        }

        System.out.println(x);

        return array;
    }

    public int randomType(){
        Random r = new Random();
        int Low = 1;
        int High = 9;
        int R = r.nextInt(High-Low) + Low;

        return R;
    }
}
