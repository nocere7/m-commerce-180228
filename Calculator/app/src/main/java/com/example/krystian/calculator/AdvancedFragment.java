package com.example.krystian.calculator;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class AdvancedFragment extends Fragment {

    public String lastSign = "";
    public boolean last = false;
    public float tmp = 0;
    public float tmp2;
    public String regex = "\\s*[-,+,/,*,^]+\\s*";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_advanced, container, false);

        //number buttons
        Button zero = (Button) view.findViewById(R.id.zero);
        Button one = (Button) view.findViewById(R.id.one);
        Button two = (Button) view.findViewById(R.id.two);
        Button three = (Button) view.findViewById(R.id.three);
        Button four = (Button) view.findViewById(R.id.four);
        Button five = (Button) view.findViewById(R.id.five);
        Button six = (Button) view.findViewById(R.id.six);
        Button seven = (Button) view.findViewById(R.id.seven);
        Button eight = (Button) view.findViewById(R.id.eight);
        Button nine = (Button) view.findViewById(R.id.nine);

        //backspace
        Button bksp = (Button) view.findViewById(R.id.bksp);

        //clear
        Button clear = (Button) view.findViewById(R.id.c);

        //dot
        Button dot = (Button) view.findViewById(R.id.dot);

        //math buttons
        Button plusminus = (Button) view.findViewById(R.id.plusminus);
        Button plus = (Button) view.findViewById(R.id.plus);
        Button minus = (Button) view.findViewById(R.id.minus);
        Button division = (Button) view.findViewById(R.id.division);
        Button multiplication = (Button) view.findViewById(R.id.multiply);
        Button equal = (Button) view.findViewById(R.id.equal);

        //advanced buttons
        Button sin = (Button) view.findViewById(R.id.sin);
        Button cos = (Button) view.findViewById(R.id.cos);
        Button tan = (Button) view.findViewById(R.id.tan);
        Button ln = (Button) view.findViewById(R.id.ln);
        Button sqrt = (Button) view.findViewById(R.id.sqrt);
        Button pow = (Button) view.findViewById(R.id.exponentiation2);
        Button powX = (Button) view.findViewById(R.id.xExponentiationY);
        Button log = (Button) view.findViewById(R.id.log);

        //number buttons click
        zero.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetToInput(view, "0");  }
        });
        one.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetToInput(view, "1");  }
        });
        two.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetToInput(view, "2");  }
        });
        three.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetToInput(view, "3");  }
        });
        four.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetToInput(view, "4");  }
        });
        five.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetToInput(view, "5");  }
        });
        six.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetToInput(view, "6");  }
        });
        seven.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetToInput(view, "7");  }
        });
        eight.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetToInput(view, "8");  }
        });
        nine.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetToInput(view, "9");  }
        });

        //backspace button click
        bksp.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   Backspace(view);  }
        });

        //clear button click
        clear.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   Clear(view);  }
        });

        //dot button click
        dot.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   Dot(view);  }
        });

        //math buttons click
        plusminus.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   Negation(view);  }
        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetmMathematicalSign(view,"+");  }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetmMathematicalSign(view,"-");  }
        });
        division.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetmMathematicalSign(view,"/"); }
        });
        multiplication.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetmMathematicalSign(view,"*"); }
        });
        powX.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   SetmMathematicalSign(view,"^"); }
        });
        equal.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   Equal(view); }
        });

        //advanced buttons click
        sin.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   Advanced(view,"sin");  }
        });

        cos.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   Advanced(view,"cos");  }
        });

        tan.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   Advanced(view,"tan");  }
        });

        ln.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   Advanced(view,"ln");  }
        });

        sqrt.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   Advanced(view,"sqrt");  }
        });

        pow.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   Advanced(view,"pow");  }
        });

        log.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {   Advanced(view,"log");  }
        });

        return view;
    }

    public void SetToInput(View view, String number){

        TextView input = (TextView) view.findViewById(R.id.input);
        String val = (String) input.getText();

        input.setText(val+number);

        last = false;
    }

    public void Backspace(View view){
        TextView input = (TextView) view.findViewById(R.id.input);
        String val = (String) input.getText();

        if(val.length() > 1)
            input.setText(val.substring(0, val.length() - 1));
        else
            input.setText("");

    }

    public void Clear(View view){
        TextView input = (TextView) view.findViewById(R.id.input);
        input.setText("");
        tmp = 0;
        lastSign = "";
    }

    public void Dot(View view){
        TextView input = (TextView) view.findViewById(R.id.input);
        String val = (String) input.getText();

        if(isNumeric(val.substring(val.length() - 1))){
            input.setText(val+'.');
        }
    }

    public void Negation(View view){
        TextView input = (TextView) view.findViewById(R.id.input);

        String val = (String) input.getText();
        float valFloat = Float.parseFloat(val);

        if(valFloat > 0)
            input.setText("-"+val);
        else{
            input.setText(val.substring(1, val.length()));
        }
    }


    public void Equal(View view) {
        TextView input = (TextView) view.findViewById(R.id.input);
        String val = (String) input.getText();

        if(isNumeric(val.substring(val.length() - 1))){
            String parts[] = val.split(regex);
            if(parts.length > 1){

                double tmpX = 0;
                switch (lastSign) {
                    case "-":
                        tmpX = Double.parseDouble(parts[0])-Double.parseDouble(parts[1]);
                        break;

                    case "+":
                        tmpX = Double.parseDouble(parts[0])+Double.parseDouble(parts[1]);
                        break;

                    case "*":
                        tmpX = Double.parseDouble(parts[0])*Double.parseDouble(parts[1]);
                        break;

                    case "/":
                        tmpX = Double.parseDouble(parts[0])/Double.parseDouble(parts[1]);
                        break;

                    case "^":
                        tmpX = Math.pow(Double.parseDouble(parts[0]),Double.parseDouble(parts[1]));
                        break;
                }
                input.setText(Double.toString(tmpX));

            }

            lastSign = "";
        }
    }

    public void SetmMathematicalSign(View view, String sign) {
        TextView input = (TextView) view.findViewById(R.id.input);
        String val = (String) input.getText();

        if(isNumeric(val.substring(val.length() - 1))){
            String parts[] = val.split(regex);
            if(parts.length > 1){

                double tmpX;
                switch (lastSign) {
                    case "-":
                        tmpX = Double.parseDouble(parts[0])-Double.parseDouble(parts[1]);
                        input.setText(Double.toString(tmpX)+sign);
                        break;

                    case "+":
                        tmpX = Double.parseDouble(parts[0])+Double.parseDouble(parts[1]);
                        input.setText(Double.toString(tmpX)+sign);
                        break;

                    case "*":
                        tmpX = Double.parseDouble(parts[0])*Double.parseDouble(parts[1]);
                        input.setText(Double.toString(tmpX)+sign);
                        break;

                    case "/":
                        tmpX = Double.parseDouble(parts[0])/Double.parseDouble(parts[1]);
                        input.setText(Double.toString(tmpX)+sign);
                        break;

                    case "^":
                        tmpX = Math.pow(Double.parseDouble(parts[0]),Double.parseDouble(parts[1]));
                        input.setText(Double.toString(tmpX)+sign);
                        break;
                }

            }
            else{
                input.setText(val+sign);
            }

            lastSign = sign;
        }

    }

    public static boolean isNumeric(String str)
    {
        try{
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    public void Advanced(View view, String type){

        Equal(view);

        TextView input = (TextView) view.findViewById(R.id.input);
        String val = (String) input.getText();

        if(isNumeric(val.substring(val.length() - 1))){
            double tmp = Double.parseDouble(val);
            switch (type) {
                case "sin":
                    tmp = Math.sin(tmp);
                    break;

                case "cos":
                    tmp = Math.cos(tmp);
                    break;

                case "tan":
                    tmp = Math.tan(tmp);
                    break;

                case "ln":
                    tmp = Math.log10(tmp);
                    break;

                case "sqrt":
                    tmp = Math.sqrt(tmp);
                    break;

                case "pow":
                    tmp = Math.pow(tmp,2);
                    break;

                case "log":
                    tmp = Math.log(tmp);
                    break;

            }
            input.setText(Double.toString(tmp));
        }
    }

}
