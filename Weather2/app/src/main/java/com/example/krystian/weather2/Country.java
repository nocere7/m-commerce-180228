package com.example.krystian.weather2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krystian on 2015-05-14.
 */
public class Country {

   public void GetCity(List<String> list, String country) {

       int i = list.indexOf(country);

       City cities = new City();
       Continent Continents = new Continent();
       System.out.println("country"+ country+" ");

       //Continents.RestartList(list);

       List<String> tmp = new ArrayList<String>();

       if(country.equals("- Polska")) {
           Continents.GetCountry(list, "Europa");
           tmp = cities.Cities(list, "- Polska");
       }
       else if(country.equals("- Niemcy")) {
           Continents.GetCountry(list, "Europa");
           tmp = cities.Cities(list, "- Niemcy");
       }
       else if(country.equals("- USA")) {
           Continents.GetCountry(list, "Ameryka Północna");
           tmp = cities.Cities(list, "- USA");
       }
       else if(country.equals("- Japonia")) {
           Continents.GetCountry(list, "Azja");
           tmp = cities.Cities(list, "- Japonia");
       }

       if(!tmp.isEmpty()){
           for(String s : tmp) {
               System.out.println(s);
               list.add(i++, s);
           }
       }

   }
}
