package com.example.krystian.weather2;

/**
 * Created by Krystian on 2015-05-23.
 */
public class Weather {

    public float temperature;
    public int humidity;
    public String icon;
    public String city;
    public String country;

    public byte[] iconData;


}