package com.example.krystian.weather2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krystian on 2015-05-14.
 */
public class City {

    public List<ArrayList<String>> ArrayCities = new ArrayList<ArrayList<String>>();

    public List<String> Cities(List<String> list, String name){

        ArrayList<String> tmp = new ArrayList<String>();

        if(name.equals("- Polska")) {             //POLSKA
            tmp.add("Lodz,PL");
            tmp.add("Warszawa,PL");
            tmp.add("Kalisz,PL");
            tmp.add("Opole,PL");
        }
        else if(name.equals("- Niemcy")){         //NIEMCY
            tmp.add("Berlin,DE");
            tmp.add("Frankfurt,DE");
        }
        else if(name.equals("- USA")){        //USA
            tmp.add("New York,US");
            tmp.add("Detroit,US");
        }
        /*else if(name.equals("- Japonia")){        //JAPONIA
            tmp.add("Tokyo,JP");
        }*/

        return tmp;

    }

    public List<String> getCities(List<String> list, String name){


        System.out.println("NAZWAAAAAA: "+ name);

        List<String> tmp = new ArrayList<String>();
        List<String> cities = new ArrayList<String>();


        tmp = Cities(list, name);

        for(String s : tmp) {
            System.out.println(s);

            cities.add(s);
        }


        return cities;
    }
}
